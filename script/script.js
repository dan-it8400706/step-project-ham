const servicesItems = document.querySelectorAll('.service-item');
const servicesContent = document.querySelectorAll('.service-img-text-item');
servicesItems.forEach(elem => {
    elem.addEventListener('click', function(ev){
        let data = ev.target.dataset.item;
        document.querySelector('.active').classList.remove('active');
        document.querySelector('.active').classList.add('no-active');
        document.querySelector('.active').classList.remove('active');
        document.querySelector(`#item${data}`).classList.add('active');
        document.querySelector(`#item${data}`).classList.remove('no-active');
        ev.target.classList.add('active');
    })
})

// --------------our works------------


// let itemWorks = document.querySelectorAll('[data-item]');
// let workImage = document.querySelectorAll('.work-image');
document.querySelectorAll('[data-item]').forEach(elem => {
    elem.addEventListener('click', function (event){
        let i = event.target.dataset.item;
        document.querySelector('.work-item-active').classList.add('work-item-no-active');
        document.querySelector('.work-item-active').classList.remove('work-item-active');
        event.target.classList.remove('.work-image-no-active');
        event.target.classList.add('work-item-active');

        document.querySelectorAll('.work-image').forEach(elem =>{
            if(i === 'all'){
                elem.classList.add('active');
                elem.classList.remove('no-active')
            }else if (elem.dataset.workImage != `${i}`){
                elem.classList.add('no-active');
                elem.classList.remove('active');
            }else{
                elem.classList.add('active');
                elem.classList.remove('no-active')
            }
        })
}
)
})
// --------------add cards----------------

// const btn = document.querySelector('.link-more');
// const workImages = document.querySelector('.work-images');
// btn.addEventListener('click', addMore)

// let arrImages =[
//     {
//     property : 'Graphic Design',
//     src : ["./images/our-works-img/graphic-design/graphic-design3.jpg",
//         //    "./images/our-works-img/graphic-design/graphic-design4.jpg",
//         //    "./images/our-works-img/graphic-design/graphic-design5.jpg"
//           ],
//     dataItem : 'graphic'
//     },

//     {
//     property : 'Web Design',
//     src : ["./images/our-works-img/web-design/web-design1.jpg", 
//         //    "./images/our-works-img/web-design/web-design2.jpg",
//         //    "./images/our-works-img/web-design/web-design3.jpg"
//         ],
//     dataItem : 'web'
//     },

//     {
//     property : 'Wordpress',
//     src : ["./images/our-works-img/wordpress/wordpress1.jpg",
//         //    "./images/our-works-img/wordpress/wordpress2.jpg",
//         //    "./images/our-works-img/wordpress/wordpress3.jpg"
//         ],
//     dataItem : 'wordpress'
//     },

//     {
//     property : 'Landing Pages',
//     src : ["./images/our-works-img/landing-page/landing-page1.jpg",
//         //    "./images/our-works-img/landing-page/landing-page2.jpg",
//         //    "./images/our-works-img/landing-page/landing-page3.jpg"
//         ],
//     dataItem : 'landing'
//     },
// ];



// function addMore(){
//     let counter = 12;
//     let newUl = arrImages.map((object)=> { return object.src.map((img)=> {
//             return `
//             <li id = "work-image ${++counter}" class="work-image" data-work-image = "${object.dataItem}">
//                 <div class="work-img-under">
//                     <a class="work-links link" href="#">
//                         <p class="line" ></p>
//                         <div class="work-hover" >
//                             <img src="./images/work-hover.svg" alt="link-work">
//                             <h5>creative design</h5>
//                             <p class="web-disign" >${object.property}</p>
//                         </div>
//                     </a>
//                 </div>
//                 <div class="work-img-front">
//                     <img src=${img} alt="our work"> 
//                 </div>
//             </li>`}) ;
//     }) ;

//     newUl.forEach((element)=> {
//     workImages.insertAdjacentHTML("beforeend", element)
//     });
// }

// ---------------------------------------------

const btn = document.querySelector('.link-more');
const workImages = document.querySelector('.work-images');
btn.addEventListener('click', addMore)

let arrImages =[
    {
    property : 'Graphic Design',
    src : "./images/our-works-img/graphic-design/graphic-design3.jpg",
    dataItem : 'graphic'
    },
    {
        property : 'Graphic Design',
        src : "./images/our-works-img/graphic-design/graphic-design4.jpg",
        dataItem : 'graphic'
    },
    {
        property : 'Graphic Design',
        src :  "./images/our-works-img/graphic-design/graphic-design5.jpg",
        dataItem : 'graphic'
    },

    {
    property : 'Web Design',
    src : "./images/our-works-img/web-design/web-design1.jpg", 
    dataItem : 'web'
    },
    {
    property : 'Web Design',
    src : "./images/our-works-img/web-design/web-design2.jpg",
    dataItem : 'web'
    },
    {
    property : 'Web Design',
    src : "./images/our-works-img/web-design/web-design3.jpg",
    dataItem : 'web'
    },

    {
    property : 'Wordpress',
    src : "./images/our-works-img/wordpress/wordpress1.jpg",
    dataItem : 'wordpress'
    },
    {
    property : 'Wordpress',
    src : "./images/our-works-img/wordpress/wordpress2.jpg",
    dataItem : 'wordpress'
    },
    {
    property : 'Wordpress',
    src : "./images/our-works-img/wordpress/wordpress3.jpg",
    dataItem : 'wordpress'
    },

    {
    property : 'Landing Pages',
    src : "./images/our-works-img/landing-page/landing-page1.jpg",
    dataItem : 'landing'
    },
    {
    property : 'Landing Pages',
    src : "./images/our-works-img/landing-page/landing-page2.jpg",
    dataItem : 'landing'
    },
    {
    property : 'Landing Pages',
    src : "./images/our-works-img/landing-page/landing-page3.jpg",
    dataItem : 'landing'
    },
];



function addMore(){
    let counter = 12;
    let newUl = arrImages.map((object)=> {
            return `
            <li id = "work-image ${++counter}" class="work-image" data-work-image = "${object.dataItem}">
                <div class="work-img-under">
                    <a class="work-links link" href="#">
                        <p class="line" ></p>
                        <div class="work-hover" >
                            <img src="./images/work-hover.svg" alt="link-work">
                            <h5>creative design</h5>
                            <p class="web-disign" >${object.property}</p>
                        </div>
                    </a>
                </div>
                <div class="work-img-front">
                    <img src=${object.src} alt="our work"> 
                </div>
            </li>`;
    }) ;

    newUl.forEach((element)=> {
    workImages.insertAdjacentHTML("beforeend", element)
    });
}

// -----------------slaider---------------


let position = 0;
const slaiderItem = document.querySelectorAll('.slaider-window');
const left = document.querySelector('.left');
const right = document.querySelector('.right');

left.addEventListener('click', function(){
    position -= 1;
    if(position < 0){
        position = 3
    }
    slaiderItem.forEach(elem => {
        if(elem.dataset.number == `${position}`){
            console.log(elem)
            elem.style.display  =  "flex";
        } else{
            elem.style.display  =  "none";
        }
    });
    return position;
})

right.addEventListener('click', function(){
    position += 1;
    if(position > 3){
        position = 0
    }
    slaiderItem.forEach(elem => {
        if(elem.dataset.number == `${position}`){
            console.log(elem)
            elem.style.display  =  "flex";
        } else{
            elem.style.display  =  "none";
        }
    });
    return position;
})

let img = document.querySelector('.list-slaider-face')
img.addEventListener('click', function(element){
    let number = element.target.dataset.number;
    console.log(number);
    position = number;
    slaiderItem.forEach(elem => {
        if(elem.dataset.number == `${position}`){
            console.log(elem)
            elem.style.display  =  "flex";
        } else{
            elem.style.display  =  "none";
        }
    });
    return position;
})
console.log(position)